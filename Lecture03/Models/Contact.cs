﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03.Models
{
    public class Contact
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
    }
}
