﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public String Title { get; set; }
        public String Author { get; set; }
        public String CoverImage { get; set; }
    }

    public static class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();

            books.Add(new Book { BookId = 1, Title = "My First Book", Author = "Some Author", CoverImage = "Assets/book2.png" } );
            books.Add(new Book { BookId = 2, Title = "My Second Book", Author = "Some Author", CoverImage = "Assets/book1.jpg" } );

            return books;
        }
    }
}
