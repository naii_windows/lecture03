﻿using Lecture03.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03.ViewModels
{
    public class BindingViewModel
    {
        public BindingViewModel()
        {
            for (int i = 0; i < 100; i++)
            {
                var item = new ToDoItem() { Title = String.Format("Task Title {0}", i) };
                Items.Add(item);
            }
        }

        public ObservableCollection<ToDoItem> Items { get; private set; } = new ObservableCollection<ToDoItem>();
    }
}
