﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lecture03
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            //loadComponents();
        }

        private void loadComponents()
        {

            var items = new List<string>();
            items.Add("String1");
            items.Add("String2");


            var flipView = new FlipView();
            flipView.ItemsSource = items;

            flipView.SelectionChanged += FlipView_SelectionChanged;
            mainPanel.Children.Add(flipView);

        }

        private async void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await new MessageDialog("SelectionChanged to -> " + e.AddedItems.ToArray()[0]).ShowAsync();
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if(((FrameworkElement)sender).Tag.Equals("flip"))
                Frame.Navigate(typeof(BookPage));
            else if (((FrameworkElement)sender).Tag.Equals("observ"))
                Frame.Navigate(typeof(ObservableExample));
            else if (((FrameworkElement)sender).Tag.Equals("slider"))
                Frame.Navigate(typeof(SliderBinding));
            else if (((FrameworkElement)sender).Tag.Equals("bindcomp"))
                Frame.Navigate(typeof(BindingComp));
        }
    }
}
