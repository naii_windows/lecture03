﻿using Lecture03.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Lecture03
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ObservableExample : Page
    {

        private ObservableCollection<Contact> Contacts;
        public ObservableExample()
        {
            this.InitializeComponent();
            Contacts = new ObservableCollection<Contact>();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Contacts.Add(new Contact { FirstName = txtFirst.Text, LastName = txtLast.Text });
        }
    }
}
