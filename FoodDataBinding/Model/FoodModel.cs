﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDataBinding.Model
{
    public class FoodModel : INotifyPropertyChanged
    {
        public string Name { get; set; }
        private bool _isFavo;
        public bool IsFavo
        {
            get { return _isFavo; }
            set
            {
                _isFavo = value;
                RaisePropertyChanged("IsFavo");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
