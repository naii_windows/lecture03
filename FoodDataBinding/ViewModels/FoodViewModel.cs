﻿using FoodDataBinding.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDataBinding.ViewModels
{
    public class FoodViewModel
    {
        public List<FoodModel> AllFoods = new List<FoodModel>();
        public ObservableCollection<FoodModel> FavoFoods = new ObservableCollection<FoodModel>();

        public FoodViewModel()
        {
            LoadFoodData();
        }

        private void LoadFoodData()
        {
            AllFoods.Add(new FoodModel()
            {
                Name = "Apple",
                IsFavo = false
            });
            AllFoods.Add(new FoodModel()
            {
                Name = "Banana",
                IsFavo = false
            });
            AllFoods.Add(new FoodModel()
            {
                Name = "Pizza",
                IsFavo = true
            });
            AllFoods.Add(new FoodModel()
            {
                Name = "Spinach",
                IsFavo = false
            });
            AllFoods.Add(new FoodModel()
            {
                Name = "Turnip",
                IsFavo = false
            });
            foreach (FoodModel eachFood in AllFoods)
            {
                eachFood.PropertyChanged += IsFavoritePropertyChanged;
                if (eachFood.IsFavo)
                {
                    FavoFoods.Add(eachFood);
                }
            }
        }

        private void IsFavoritePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            FoodModel model = sender as FoodModel;
            if (model.IsFavo)
            {
                FavoFoods.Add(model);
            }
            else {
                FavoFoods.Remove(model);
            }
        }
    }
}
